import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import axios from "axios";
import { Button } from "@mui/material";




export const ProductCard = ({ product, ...rest }) => {
  const [data, setData] = React.useState([]);

  React.useEffect(() => {
    axios.get(`http://localhost:3004/products`)
    .then(res => {
      const persons = res.data;
      setData(persons);
    })
    .catch(error => console.log(error));
  }, []);
  const columns = [
    { field: "id", headerName: "ID", width: 20 },
    { field: "title", headerName: "Tên sản phẩm", width: 200 },
    { field: "brand", headerName: "Thương hiệu", width: 130 },
    { field: "img", headerName: "Hình ảnh", width: 230 },
    { field: "price", headerName: "Giá thành", width: 120 },
    { field: "saleOff", headerName: "Giá (%)", width: 130 },
    { field: "count", headerName: "Số lượng", width: 90 },
    { field: "action", headerName: "Tuỳ chọn", width: 160,
    renderCell: () => {

      return (
      <>
      <div style={{paddingRight: '5px'}}>
      <Button variant="contained">Sửa</Button>
      </div>
      <div>
       <Button variant="contained" color="error">Xoá</Button>
       </div>
       </>);
       
    }
  }
  ];
  return (
    <>
    <div style={{ height: 650}}>
      <DataGrid
        columns={columns}
        rows={data}
        getRowId = {(row) => row.id}
        pageSize={10}
        rowsPerPageOptions={[10]}
        checkboxSelection
        />
    </div>
  </>
);


} 
