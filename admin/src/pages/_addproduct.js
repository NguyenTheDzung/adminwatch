import { DashboardLayout } from "../components/dashboard-layout";
import { ProductListToolbar } from "../components/product/product-list-toolbar";
import { Box, Container, Grid, Typography } from "@mui/material";
import Head from "next/head";

const addProduct = (props) => {
  addProduct.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;
  return (
    <>
     <Box
    >
      <Container maxWidth={false}>
      <Box {...props}>
    <Box
      sx={{
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        m: -1
      }}
    >
      <Typography
        sx={{ m: 1 }}
        variant="h4"
      >
        Thêm sản phẩm
      </Typography>
      </Box>
      </Box>
      </Container>
    </Box>
      
    </>
  );
};
export default addProduct;
