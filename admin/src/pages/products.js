import Head from 'next/head';
import { Box, Container, Grid } from '@mui/material';
import { ProductListToolbar } from '../components/product/product-list-toolbar';
import { ProductCard } from '../components/product/product-card';
import { DashboardLayout } from '../components/dashboard-layout';

const Products = () => {

 
  Products.getLayout = (page) => (
    <DashboardLayout>
      {page}
    </DashboardLayout>
  );
  return (
  <>
    <Head>
      <title>
        Sản Phẩm | Admin
      </title>
    </Head>
    <Box
    >
      <Container maxWidth={false}>
        <ProductListToolbar />
        <Box sx={{ pt: 3 }}>
          <Grid
            container
            spacing={3}
          >
            
              <Grid
                item
               
                xs={12}
              >
                <ProductCard />
              </Grid>
            
          </Grid>
        </Box>
      </Container>
    </Box>
  </>
);
}


export default Products;
