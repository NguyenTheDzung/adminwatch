import { DashboardLayout } from "../Dashboards/DashboardLayout";
import { Box, Container, Typography } from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "../theme";
import { DashboardSidebar } from "../Dashboards/DashboardSidebar";
import { DashboardNavbar } from "../Dashboards/DashboardNavbar";


function AddProduct  (props)  {
  AddProduct.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;
  return (
    <>
    <ThemeProvider theme={theme}>
      <div className="dashBoardNarBar">
        <DashboardNavbar />
      </div>
      <DashboardSidebar />
      <div className="wrapper-AdminHome">
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            py: 8,
            backgroundColor: '#f9fafc'
          }}
        >
          <Container maxWidth={false}>
          <div className="App">
      <form>
        
        <input
          type="file"
        />
      </form>
    </div>
          </Container>
        </Box>
      </div>
    </ThemeProvider>
  </>
  );
};
export default AddProduct;
