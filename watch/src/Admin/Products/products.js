import { Box, Container, Grid } from '@mui/material';
import { ProductListToolbar } from './product-list-toolbar';
import { ProductCard } from './product-card';
import { DashboardLayout } from '../Dashboards/DashboardLayout';
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "../theme";
import { DashboardSidebar } from "../Dashboards/DashboardSidebar";
import { DashboardNavbar } from "../Dashboards/DashboardNavbar";

const Products = () => {

 
  Products.getLayout = (page) => (
    <DashboardLayout>
      {page}
    </DashboardLayout>
  );
  return (
  <>
    <ThemeProvider theme={theme}>
      <div className="dashBoardNarBar">
        <DashboardNavbar />
      </div>
      <DashboardSidebar />
      <div className="wrapper-AdminHome">
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            py: 8,
            backgroundColor: '#f9fafc'
          }}
        >
          <Container maxWidth={false}>
            <ProductListToolbar />
            <Box sx={{ mt: 3 }}>
              <ProductCard />
            </Box>
          </Container>
        </Box>
      </div>
    </ThemeProvider>
  </>
);
}


export default Products;
