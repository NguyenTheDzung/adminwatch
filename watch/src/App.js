import HomePage from './Components/Homepage/HomePage';
import DetailProduct from './Components/DetailProduct/detailProduct';
import { Routes, Route } from 'react-router-dom';
import AboutUs from './Components/AboutUs/aboutUs';
import Login from './Components/Login';
import ForgotPass from './Components/ForgotPassWord';
import SignUp from './Components/Register';
import HomePageMan from './Components/Homepage/HomPageMan';
import HomePageWoman from './Components/Homepage/HomePageWoman';
import HomePageBestSeller from './Components/Homepage/HomePageBestSeller';
import AdminHome from './Admin';
import Customers from './Admin/Customers';
import Products from './Admin/Products/products';
import AddProduct from './Admin/Products/addProduct';
import AddCustomer from './Admin/Customers/addCustomer';
import Orders from './Admin/Order';
import Accounts from './Admin/Accounts';
import AddAccount from './Admin/Accounts/addAccounts';



function App() {
  return (
    <>
      
      <Routes>
        <Route path='/' element = {<HomePage />} />
        <Route path="detail/product" element = {<DetailProduct />} />
        <Route path='aboutUs' element = { <AboutUs />} />
        <Route path='login' element = { <Login />} />
        <Route path='forgot/password' element = { <ForgotPass />} />
        <Route path='singUp' element = { <SignUp />} />
        <Route path='product/men' element = { <HomePageMan />} />
        <Route path='product/woman' element =  { <HomePageWoman />} />
        <Route path='bestseller' element={ <HomePageBestSeller /> } />
        <Route path='admin' element = { <AdminHome />} />
        <Route path='admin/customer' element= { <Customers />} />
        <Route path='admin/product' element = { <Products />} />
        <Route path='admin/product/addProduct' element = { <AddProduct /> } />
        <Route path='admin/customer/addCustomer' element = { <AddCustomer /> } />
        <Route path='admin/account/addAccount' element = { <AddAccount /> } />
        <Route path='admin/order' element = { <Orders /> } />
        <Route path='admin/account' element = { <Accounts /> } />
      </Routes>
      
    </>
  );
}

export default App;
