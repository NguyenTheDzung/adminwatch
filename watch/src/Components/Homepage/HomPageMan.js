import Header from '../Header/header';
import Infor from '../InforHeader/infor-header';
import Navbar from '../Navbar/navbar';
import Footer from '../Footer/footer';
import ContentMen from '../Content/contentMen';




function HomePageMan () {


    return(
        <>  
            <Navbar />
            <Header />
            <Infor />
            <ContentMen />
            <Footer />
        </>
    )
}
export default HomePageMan;