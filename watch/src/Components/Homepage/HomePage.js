import Header from '../Header/header';
import Infor from '../InforHeader/infor-header';
import Navbar from '../Navbar/navbar';
import Footer from '../Footer/footer';
import ManBestSeller from '../ManBestSeller';
import WomanBestSeller from '../WomanBestSeller';
import { Divider } from '@mui/material';
import Decripsiton from '../DecripstionWebsite';




function HomePage () {


    return(
        <>  
            <Navbar />
            <Header />
            <Infor />
            <ManBestSeller />
            <WomanBestSeller />
            <Decripsiton />
            <Footer />
        </>
    )
}
export default HomePage;