import {Link} from 'react-router-dom';
import { useState } from 'react';
import imgChoice from "./imgChoice";
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';
import phuKienImg from '../../Images/phukien.png';
import './navbar.css';

function DropDownMen() {
  const [menBestSeller, setMenBestSeller] = useState(true)
  const [accessory, setAccessory] = useState(false);
 
  const TrueMenBestSeller = () => {
    setAccessory(false);
    setMenBestSeller(true);
  }
  const menAccessory = () => {
    setMenBestSeller(false);
    setAccessory(true)
  }

    return (
      <div className="dropdown-contentMan"  
      onMouseLeave={TrueMenBestSeller}>
      <div className="dropdown-detail">
        <div className="menuDropDown">
          <div
            style={{
              display: "grid",
              gridRowGap: "40px",
              rowGap: "40px",
              gridAutoColumns: "1fr",
            }}
          >
            <div className="summary1-2"onMouseEnter={TrueMenBestSeller}>
            <Link to="" className="dropIndex1">
              ĐỒNG HỒ
            </Link>
            <div
              style={{
                marginTop: "20px",
                display: "grid",
                gridTemplateRows: "1fr",
                gridRowGap: "16px",
                rowGap: "16px",
              }}
            >
              <Link to="" className="dropIndex2">
                BÁN CHẠY NHẤT
              </Link>
              </div>
            </div>
            <Link to="" className="dropIndex3" onMouseEnter={menAccessory}>
              PHỤ KIỆN
            </Link>
          </div>
        </div>
        <div className="choiceDropDown">
          <div className={menBestSeller ? "showMenu" : "hideMenu"}>
          <div className="choiceDropDownChild1">
          {imgChoice.map((data) => (
            <Link key={data.title} to="" className="linkChoice">
              <span
                style={{
                  boxSizing: "border-box",
                  display: "inline-block",
                  overflow: "hidden",
                  width: "initial",
                  height: "initial",
                  backGround: "none",
                  opacity: 1,
                  border: "0px",
                  margin: "0px",
                  padding: "0px",
                  position: "relative",
                  maxWidth: "100%",
                }}
              >
                <span className="spanChild1">
                </span>
                <img className="imgChoice" src={data.img} alt="" />
              </span>
              <div className="titleChoice">{data.title}</div>
            </Link>
          ))}
          <Link to="" className="linkChoice">
            <div className="linkChoiceViewAll">
              <span style={{
                    fontSize: '14px',
                    fontStyle: 'normal',
                    fontWeight: '500',
                    lineHeight: '14px',
                    letterSpacing: '.02em',
                    textAlign: 'center',
              }}>XEM TẤT CẢ</span>
              <ArrowRightAltIcon />
            </div>
          </Link>
          </div>
          </div>
          <div className={accessory ? 'showAccessory' : 'hideAccessory'}>
          <div className="choiceDropDownChild2">
            <Link to="" className="linkChoice">
            <span
                style={{
                  boxSizing: "border-box",
                  display: "inline-block",
                  overflow: "hidden",
                  width: "initial",
                  height: "initial",
                  backGround: "none",
                  opacity: 1,
                  border: "0px",
                  margin: "0px",
                  padding: "0px",
                  position: "relative",
                  maxWidth: "100%",
                }}
              >
                <span className="spanChild1">
                </span>
                <img className="imgChoice" src={phuKienImg} alt="" />
              </span>
              <div className="titleChoice">CRUFFS</div>
            </Link>
       
          <Link to="" className="linkChoice">
            <div className="linkChoiceViewAll">
              <span style={{
                    fontSize: '14px',
                    fontStyle: 'normal',
                    fontWeight: '500',
                    lineHeight: '14px',
                    letterSpacing: '.02em',
                    textAlign: 'center',
              }}>XEM TẤT CẢ</span>
              <ArrowRightAltIcon />
            </div>
            </Link>
          </div>
          </div>
        </div>
      </div>
    </div>
    )
}

export default DropDownMen;