import "./navbar.css";
import { Link } from "react-router-dom";
import StateMenu from "../StateMenu";
import LoginIcon from "@mui/icons-material/Login";
import { useTranslation } from "react-i18next";
import { useState } from "react";
import DropDownMen from "./DropdownMen";
import DropDownWoman from "./DropdownWoman";
import AccountSetting from "../AccountSettings";
import Tooltip from '@mui/material/Tooltip';
import Cart from "../StateMenu/Cart";

function Navbar() {
  const { t } = useTranslation();

  const [show, setShow] = useState(false);
  const [showWoman, setShowWoman] = useState(false);

  const leaveHover = () => {
    setShow(false);
    setShowWoman(false);
  }
  const callHoverEventMen = () => {
    setShow(true);
    setShowWoman(false);
  }

  const callHoverEventWoman = () => {
    setShow(false);
    setShowWoman(true);
  }

  return (
    <>
    <div className={show ? "showMenu" : "hideMenu"} onMouseLeave={leaveHover}>
      <DropDownMen />
    </div>
    <div className={showWoman ? "showMenuWoman" : "hideMenuWoman"} onMouseLeave={leaveHover}>
      <DropDownWoman />
    </div>
      <div className=" container-fluid overNav">
        <div className="row">
          <div className="col-4 nav justify-content-left left">
            <StateMenu />
            <li className="nav-item menu  dropdown men" onMouseEnter={callHoverEventMen}
             >
              <Link
                to="/"
                className="nav-link active"
                aria-current="page"
                href="#"
              >
                {t("men")}
              </Link>
            </li>
            <li className="nav-item menu dropdown woman" onMouseEnter={callHoverEventWoman}>
              <Link
                to="/"
                className="nav-link active"
                aria-current="page"
                href="#"
              >
                {t("woman")}
              </Link>
            </li>
            <li className="nav-item menu dropdown" onMouseEnter={leaveHover}>
              <Link
                to="/aboutUs"
                className="nav-link active"
                aria-current="page"
                href="#"
              >
                {t("we")}
              </Link>
            </li>
          </div>
          <div className="col-4 text-center logo">
            <Link className="nav-link active" aria-current="page" to="/">
              <img
                src="https://curnonwatch.com/_next/static/media/logo.cc5d661a.svg"
                alt=""
              ></img>
            </Link>
          </div>
          <div className="col-4 text-end nav justify-content-end right">
            <li className="nav-item">
              <Link
                className="nav-link active"
                id="one"
                aria-current="page"
                to=""
                alt=""
              >
                {/* {t("cart")} */}
                <span className="nav-icon">
                  <Cart />
                </span>
              </Link>
            </li>
            <li className="nav-item">
              <Link
                className="nav-link active"
                id="two"
                aria-current="page"
                to=""
              >
                <span className="nav-icon">
                  <Cart />
                </span>
              </Link>
            </li>
            <li className="nav-item">
              <Tooltip title="Đăng Nhập">
              <Link
                className="nav-link active"
                id="three"
                aria-current="page"
                to="/login"
              >
                LOGIN
                <span className="nav-icon">
                  <LoginIcon />
                </span>
              </Link>
              </Tooltip>
            </li>
            <li className="nav-item">
              <Tooltip title="Đăng Nhập">
              <Link
                className="nav-link active"
                id="four"
                aria-current="page"
                to="/login"
              >
                <span className="nav-icon">
                  <LoginIcon />
                </span>
              </Link>
              </Tooltip>
            </li>
            <li className="nav-item">
              <div className="nav-link active">
                {/* <AccountSetting /> */}
              </div>
            </li>
          </div>
        </div>
      </div>
    </>
  );
}

export default Navbar;
