import * as React from "react";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import Button from "@mui/material/Button";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import CloseIcon from "@mui/icons-material/Close";
import Infor from '../InforHeader/infor-header';
import imgTest from '../../Images/7.jpg'
import "./cart.css";

export default function Cart() {
  const [state, setState] = React.useState({
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div>
      <Box
        sx={{ width: "80vw" }}
        role="presentation"
        >
        <div className="wrapperCart">
          <div className="headerCart">
            <div className="titleCart">GIỎ HÀNG CỦA BẠN</div>
            <div>
              <Button>
                <CloseIcon onClick={toggleDrawer(anchor, false)} fontSize="large" sx={{ color: "white" }} />
              </Button>
            </div>
          </div>
          <div>
            <Infor />
          </div>
          <div className="miniCart">
            <div className="row">
              <div className="col-4">
                <div style={{ position: 'absolute', cursor: 'pointer'}}>
                  <button style={{border: 'none', backgroundColor: 'unset'}}>
                    <CloseIcon sx={{backgroundColor: 'gray'}} fontSize="small" />
                    </button>
                </div>
                <div className="imgProductCart">
                  <img src={imgTest} alt="" />
                </div>
              </div>
              <div className="col-5">
                <div style={{top : '0'}}>ROLEX</div>
                <div style={{marginTop: '2rem'}}>40mm</div>
              </div>
              <div className="col-3">3</div>
            </div>
          </div>
        </div>
      </Box>
    </div>
  );

  return (
    <div style={{ display: "inline" }}>
      {["right"].map((anchor) => (
        <React.Fragment key={anchor}>
          <Button onClick={toggleDrawer(anchor, true)}>
            <ShoppingCartIcon sx={{ color: "black", fontSize: "25px" }} />
          </Button>
          <Drawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}
